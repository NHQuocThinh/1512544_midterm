import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { storage } from "../../Config/fbConfig";
import { firestoreConnect } from "react-redux-firebase";
import { updateMessage, sendMessage, updateImageUpload } from "../../Actions";
import Message from "../../Components/Message/Message";
import MessageList from "../../Components/MessageList/MessageList";
import "./ChattingStyle.css";

class Chatting extends React.Component {
  getChatList = () => {
    if (this.props.chat) {
      const user1 = this.props.MessageReducer.from;
      const user2 = this.props.MessageReducer.to;
      var result = this.props.chat.filter(item => {
        return (
          (item.user1 === user1 && item.user2 === user2) ||
          (item.user1 === user2 && item.user2 === user1)
        );
        // return (
        //   (item.user1 === "quocthinhag97@gmail.com" && item.user2 === user2) ||
        //   (item.user1 === user2 && item.user2 === "quocthinhag97@gmail.com")
        // );
      });
      var ui = result.map(item => {
        return (
          <li>
            <MessageList
              user={item.user1}
              mess={item.message}
              currentUser={this.props.currentUser}
            />
            {/* <span>{item.message}</span> */}
          </li>
        );
      });
      return ui;
    }
  };

  uploadImageToFirebase = (data) => {
    
  }

  render() {
    var chatlist = this.getChatList();
    return (
      <div className="chatting_Container">
        <div className="chatting_MessageList">
          <ul>{chatlist}</ul>
        </div>
        <div className="chatting_Line" />
        <Message
          updateMessage={this.props.updateMessage}
          sendMessage={this.props.sendMessage}
          message={this.props.MessageReducer.message}
          updateImageUpload={this.props.updateImageUpload}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.AuthReducer.email,
    chat: state.firestore.ordered.chat,
    MessageReducer: state.MessageReducer,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateMessage: data => dispatch(updateMessage(data)),
    sendMessage: () => dispatch(sendMessage()),
    updateImageUpload: data => dispatch(updateImageUpload(data)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect([{ collection: "chat", orderBy: "sendTime" }])
)(Chatting);
