import React from "react";
import { connect } from "react-redux";
import MyHeader from "../MyHeader/MyHeader";
import FriendList from "../FriendList/FriendList";
import Chatting from "../Chatting/Chatting";
import UploadingImage from "../../Components/UploadingImage/UploadingImage";

class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <MyHeader />
        <FriendList />
        <Chatting />
        {this.props.progress !== 0 && <UploadingImage />}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    progress: state.MessageReducer.uploadingPercent,
  };
};

export default connect(
  mapStateToProps,
)(Dashboard);
