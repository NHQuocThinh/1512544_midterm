import React from "react";
import { NavLink, withRouter } from "react-router-dom";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import { connect } from "react-redux";

import { userLogout } from "../../Actions";

class MyHeader extends React.Component {
  logoutButtonOnClick = () => {
    if (this.props.onlineList) {
      const userId = this.props.onlineList.filter(item => {
        return item.email === this.props.email;
      });
      if (userId) {
        this.props.userLogout(userId[0].id, userId[0].email);
        this.props.history.push({ pathname: "/" });
      }
    }
  };

  render() {
    return (
      <div
        style={{
          position: "absolute",
          height: 100,
          width: "100%",
          backgroundColor: "#444753",
          alignItems: "center",
          display: "flex",
        }}
      >
        <span
          style={{
            marginLeft: 300,
            color: "white",
            fontSize: 50,
            fontWeight: "500",
            letterSpacing: 10,
          }}
        >
          TALK WITH ME
        </span>
        <button
          onClick={this.logoutButtonOnClick}
          style={{
            marginLeft: 750,
            backgroundColor: "#444753",
            border: "none",
            fontWeight: "500",
            letterSpacing: 5,
            color: 'white'
          }}
        >
          LOG OUT
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    email: state.AuthReducer.email,
    onlineList: state.firestore.ordered.online,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    userLogout: (id, email) => dispatch(userLogout(id, email)),
  };
};

export default withRouter(
  compose(
    connect(
      mapStateToProps,
      mapDispatchToProps
    ),
    firestoreConnect([{ collection: "online" }])
  )(MyHeader)
);
