import React from "react";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import { compose } from "redux";
import googleLoginImage from '../../image/googleLogin.png'
import LoginForm from "../../Components/LoginForm/LoginForm";
import SignUpForm from "../../Components/SignUpForm/SignUpForm";
import {
  changeTab,
  createAccount,
  updatePassword,
  updateUsername,
  userLogin,
} from "../../Actions";
import "./LoginContainerStyle.css";

class LoginContainer extends React.Component {
  loginTabOnClick = () => {
    this.props.clickingLoginTab();
  };

  signUpTabOnClick = () => {
    this.props.clickingSignUpTab();
  };

  loginWithGoogleOnclick = () => {};

  signUpButtonOnclick = () => {
    const data = {
      username: this.props.username,
      password: this.props.password,
      status: "offline",
    };
    this.props.createAccount(data);
  };

  loginButtonOnClick = () => {
    const data = {
      username: this.props.username,
      password: this.props.password,
      status: "online",
    };
    this.props.login(data);
  };

  updateUsernameOnChange = data => {
    this.props.updateUsername(data);
  };

  updatePasswordOnChange = data => {
    this.props.updatePassword(data);
  };

  loginWithGoogleButtonClick = () => {
    this.props.userLogin(this.props.usersData, this.props.offlineList);
  };

  componentDidUpdate = () => {
    if (this.props.status === "online") {
      this.props.history.push({
        pathname: "/dashboard",
      });
    }
  };

  render() {
    return (
      <div className="containerLoginContainer">
        <div className="windowLoginAndSignUp">
          <div className="buttonLoginAndSignUpContainer">
            <button
              className="loginButtonContainer"
              style={{
                color: this.props.loginTabColor === "white" ? "black" : "white",
                backgroundColor: this.props.loginTabColor,
              }}
              onClick={this.loginTabOnClick}
            >
              LOG IN
            </button>
            <button
              className="signUpButtonContainer"
              style={{
                color:
                  this.props.signUpTabColor === "white" ? "black" : "white",
                backgroundColor: this.props.signUpTabColor,
              }}
              onClick={this.signUpTabOnClick}
            >
              SIGN UP
            </button>
          </div>
          {this.props.loginTabColor === "white" ? (
            <LoginForm />
          ) : (
            <SignUpForm
              signUpButtonOnclick={this.signUpButtonOnclick}
              updateUsernameOnChange={this.updateUsernameOnChange}
              updatePasswordOnChange={this.updatePasswordOnChange}
            />
          )}
          {this.props.loginTabColor === "white" && (
            <p className="orLine">----or----</p>
          )}
          {this.props.loginTabColor === "white" && (
            // <div class="btn white darken-4 col s10 m4">
            //   <a onClick={this.loginWithGoogleButtonClick}>
            //     <div class="left">
            //       <img
            //         width="20px"
            //         alt="Google &quot;G&quot; Logo"
            //         src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/512px-Google_%22G%22_Logo.svg.png"
            //       />
            //     </div>
            //     Login with Google
            //   </a>
            // </div>
            <img src={googleLoginImage} style={{width: 200,}} onClick={this.loginWithGoogleButtonClick} alt={"google-login-button"}/>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.AuthReducer,
    usersData: state.firestore.ordered.users,
    offlineList: state.firestore.ordered.offline,
    status: state.AuthReducer.status,
    loginTabColor: state.LoginReducer.loginTabColor,
    signUpTabColor: state.LoginReducer.signUpTabColor,
    username: state.LoginReducer.username,
    password: state.LoginReducer.password,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    clickingLoginTab: () => dispatch(changeTab("login")),
    clickingSignUpTab: () => dispatch(changeTab("signup")),
    createAccount: data => dispatch(createAccount(data)),
    updateUsername: data => dispatch(updateUsername(data)),
    updatePassword: data => dispatch(updatePassword(data)),
    userLogin: (userData, offlineList) =>
      dispatch(userLogin(userData, offlineList)),
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect([{ collection: "users" }, { collection: "offline" }])
)(LoginContainer);
