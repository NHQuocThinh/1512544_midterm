import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { firestoreConnect } from "react-redux-firebase";
import "./FriendListStyle.css";
import FriendListItem from "../../Components/FriendListItem/FriendListItem";
import SearchFriend from "../../Components/SearchFriend/SearchFriend";
import { beginChat, loadFriendList, updateSearchFriendData, searchFriend } from "../../Actions";

class FriendList extends React.Component {
  initiateFriendList = () => {
    console.log(this.props.usersData);
    if (this.props.usersData && this.props.online && this.props.offline) {
      var result = [];
      for (let i = 0; i < this.props.usersData.length; i++) {
        var email = this.props.usersData[i].email;
        var status = "";
        for (let j = 0; j < this.props.online.length; j++) {
          if (email === this.props.online[j].email) {
            status = "online";
            break;
          }
        }
        if (status === "") status = "offline";
        var data = {
          id: this.props.usersData[i].id,
          email: email,
          status: status,
          name: this.props.usersData[i].displayName,
          photo: this.props.usersData[i].photoURL,
        };
        if((this.props.searchData !== "" && data.name.indexOf(this.props.searchData) !== -1)||(this.props.searchData===''))
          result.push(data);
      }
      this.props.loadFriendList(result);
      var ui = result.map(item => {
        return (
          <li
            onClick={() => {
              this.props.beginChat(this.props.auth.email, item.email);
            }}
          >
            <FriendListItem
              name={item.name}
              email={item.email}
              status={item.status}
              photo={item.photo}
              toUser={this.props.to}
            />
          </li>
        );
      });
      return ui;
    }
  };

  render() {
    var userFriendList = this.initiateFriendList();
    return (
      <div className="friendList_Container">
        <SearchFriend updateSearchFriendData={this.props.updateSearchFriendData}/>
        <div className="friendList_List">
          <ul>{userFriendList}</ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    usersData: state.firestore.ordered.users,
    online: state.firestore.ordered.online,
    offline: state.firestore.ordered.offline,
    auth: state.AuthReducer,
    searchData: state.FriendListReducer.searchData,
    to: state.MessageReducer.to,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    beginChat: (from, to) => dispatch(beginChat(from, to)),
    loadFriendList: (data) => dispatch(loadFriendList(data)),
    updateSearchFriendData: (data) => {
      dispatch(updateSearchFriendData(data))
      dispatch(searchFriend());
    },
  };
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect([
    { collection: "users" },
    { collection: "online" },
    { collection: "offline" },
    { collection: "chat", orderBy: "sendTime" },
  ])
)(FriendList);
