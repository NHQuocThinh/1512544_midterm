const initialState = {
  email: '',
  displayName: '',
  uid: '',
  photoURL: '',
  status: 'offline'
};

const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case "AuthReducer_loginSuccess": {
      return {
        ...state,
        email: action.data.email,
        displayName: action.data.displayName,
        uid: action.data.uid,
        photoURL: action.data.photoURL,
        status: 'online'
      };
    }
    case "AuthReducer_Logout": {
      return {
        ...state,
        email: '',
        displayName: '',
        uid: '',
        photoURL: '',
        status: 'offline'
      };
    }
    default:
      return state;
  }
};

export default AuthReducer;
