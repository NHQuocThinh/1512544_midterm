const initialState = {
  from: "",
  to: "",
  message: "",
  uploadImage: "",
  uploadingPercent: 0,
};

const MessageReducer = (state = initialState, action) => {
  switch (action.type) {
    case "MessageReducer_BeginChat": {
      return {
        ...state,
        from: action.from,
        to: action.to,
      };
    }
    case "MessageReducer_UpdateMessage": {
      return {
        ...state,
        message: action.data,
      };
    }
    case "MessageReducer_SendMessage": {
      return {
        ...state,
        message: "",
        uploadImage: "",
        uploadingPercent: 0,
      };
    }
    case "MessageReducer_UpdateImageUpload": {
      return {
        ...state,
        uploadImage: action.data,
      };
    }
    case "MessageReducer_updateProgress": {
      return {
        ...state,
        uploadingPercent: action.data,
      };
    }
    default:
      return state;
  }
};

export default MessageReducer;
