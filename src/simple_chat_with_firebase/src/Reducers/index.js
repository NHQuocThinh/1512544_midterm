import { combineReducers } from "redux";
import { firestoreReducer } from "redux-firestore";
import { firebaseReducer } from "react-redux-firebase";
import LoginReducer from "./LoginReducer";
import AuthReducer from "./AuthReducer";
import MessageReducer from "./MessageReducer";
import FriendListReducer from "./FriendListReducer"

export default combineReducers({
  LoginReducer,
  AuthReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer,
  MessageReducer,
  FriendListReducer,
});
