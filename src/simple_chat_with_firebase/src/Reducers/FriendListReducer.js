const initialState = {
  databaseData: [],
  data: [],
  searchData: "",
};

const FriendListReducer = (state = initialState, action) => {
  switch (action.type) {
    case "FriendListReducer_LOAD_FRIEND_LIST": {
      return {
        ...state,
        data: action.data,
        databaseData: action.data,
      };
    }
    case "FriendListReducer_UPDATE_SEARCH_DATA": {
      return {
        ...state,
        searchData: action.data,
      };
    }
    case "FriendListReducer_SEARCHING": {
      return {
        ...state,
        data: action.data,
      };
    }
    default:
      return state;
  }
};

export default FriendListReducer;
