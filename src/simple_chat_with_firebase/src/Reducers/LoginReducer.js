const initialState = {
  loginTabColor: "white",
  signUpTabColor: "#E38968",
  username: "",
  password: "",
  status: 'offline'
};

const LoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case "LoginReducer_ChangeTab": {
      return {
        ...state,
        loginTabColor: action.data === "login" ? "white" : "#86BB71",
        signUpTabColor: action.data !== "login" ? "white" : "#E38968",
      };
    }
    case "LoginReducer_UpdateUsername": {
      return {
        ...state,
        username: action.data,
      };
    }
    case "LoginReducer_UpdatePassword": {
      return {
        ...state,
        password: action.data,
      };
    }
    default:
      return state;
  }
};

export default LoginReducer;
