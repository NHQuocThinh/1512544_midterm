import React, { Component } from "react";
import { BrowserRouter as Router, Route } from 'react-router-dom';
import MyHeader from './Containers/MyHeader/MyHeader'
import LoginContainer from "./Containers/LoginContainer/LoginContainer";
import Dashboard from "./Containers/Dashboard/Dashboard";

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={LoginContainer}/>
          <Route exact path="/dashboard" component={Dashboard}/>
        </div>
      </Router>
    );
  }
}

export default App;
