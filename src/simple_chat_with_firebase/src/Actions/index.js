import * as firebase from "firebase";
import { storage } from "../Config/fbConfig";

export const changeTab = input => {
  return dispatch => {
    dispatch({
      type: "LoginReducer_ChangeTab",
      data: input,
    });
  };
};

export const updateUsername = data => {
  return dispatch => {
    dispatch({
      type: "LoginReducer_UpdateUsername",
      data: data,
    });
  };
};

export const updatePassword = data => {
  return dispatch => {
    dispatch({
      type: "LoginReducer_UpdatePassword",
      data: data,
    });
  };
};

export const createAccount = data => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    alert(JSON.stringify(data));
    const firestore = getFirestore();
    firestore
      .collection("users")
      .add({
        ...data,
        createAt: new Date(),
      })
      .then(() => {
        dispatch({
          type: "LoginReducer_CreateAccount",
          data: data,
        });
      });
  };
};

function checkAccountIfExist(usersData, data) {
  const result = usersData.filter(item => {
    return item.email === data.email;
  });
  return result;
}

function getUserOutOffline(offlineList, email) {
  const result = offlineList.filter(item => {
    return item.data.email === email;
  });
  return result[0].id;
}

export const userLogin = (usersData, offlineList) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase
      .auth()
      .signInWithPopup(provider)
      .then(function(result) {
        var data = {
          email: result.user.email,
          displayName: result.user.displayName,
          uid: result.user.uid,
          photoURL: result.user.photoURL,
          status: "offline",
        };
        const firestore = getFirestore();
        firestore
          .collection("users")
          .get()
          .then(snapshot => {
            const result = snapshot.docs.map(item => {
              return item.data();
            });
            var existArray = checkAccountIfExist(result, data);
            if (existArray.length === 0) {
              console.log("New account");
              const firestore = getFirestore();
              firestore
                .collection("users")
                .add({
                  ...data,
                  createAt: new Date(),
                })
                .then(() => {
                  dispatch({
                    type: "LoginReducer_CreateAccount",
                    data: data,
                  });
                });
              firestore.collection("online").add({
                email: data.email,
              });
              dispatch({
                type: "AuthReducer_loginSuccess",
                data: data,
              });
            } else {
              console.log("Exist account");
              firestore
                .collection("offline")
                .get()
                .then(snapshot => {
                  const result = snapshot.docs.map(item => {
                    return {
                      data: item.data(),
                      id: item.id,
                    };
                  });
                  var offlineId = getUserOutOffline(result, data.email);
                  const firestore = getFirestore();
                  firestore.collection("online").add({
                    email: data.email,
                  });
                  firestore
                    .collection("offline")
                    .doc(offlineId)
                    .delete();
                  dispatch({
                    type: "AuthReducer_loginSuccess",
                    data: data,
                  });
                });
            }
          });
      })
      .catch(function(error) {});
  };
};

export const userLogout = (id, email) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const firebase = getFirebase();
    const firestore = getFirestore();
    firestore
      .collection("online")
      .get()
      .then(snapshot => {
        const tmp = snapshot.docs.map(item => {
          return {
            data: item.data().email,
            id: item.id,
          };
        });
        console.log(tmp);
        const result = tmp.filter(item => {
          return item.data === email;
        });
        console.log(result[0].id);
        firestore
          .collection("online")
          .doc(result[0].id)
          .delete().then(()=>{
            console.log("OK")
          }).catch((e)=>{
            console.log(e);
          });
        firestore.collection("offline").add({
          email: result[0].data,
        });
        dispatch({
          type: "AuthReducer_Logout",
        });
      });
  };
};

export const updateMessage = data => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({
      type: "MessageReducer_UpdateMessage",
      data: data,
    });
  };
};

export const updateImageUpload = data => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({
      type: "MessageReducer_UpdateImageUpload",
      data: data,
    });
  };
};

export const sendMessage = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const message = getState().MessageReducer.message;
    const from = getState().MessageReducer.from;
    const to = getState().MessageReducer.to;
    const imageUpload = getState().MessageReducer.uploadImage;
    const firestore = getFirestore();
    if (imageUpload !== "") {
      const uploadTask = storage
        .ref(`images/${imageUpload.name}`)
        .put(imageUpload);
      uploadTask.on(
        "state_changed",
        snapshot => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          console.log(progress);
          dispatch({
            type: "MessageReducer_updateProgress",
            data: progress,
          });
        },
        error => {
          console.log(error);
        },
        () => {
          storage
            .ref("images")
            .child(imageUpload.name)
            .getDownloadURL()
            .then(url => {
              const data = "ImageLink__" + url;
              firestore.collection("chat").add({
                user1: from,
                user2: to,
                message: data,
                sendTime: new Date(),
              });
              document.getElementById("uploadImage").value = "";
            });
          dispatch({
            type: "MessageReducer_updateProgress",
            data: 0,
          });
        }
      );
    }
    if (message !== "") {
      firestore.collection("chat").add({
        user1: from,
        user2: to,
        message: message,
        sendTime: new Date(),
      });
    }
    dispatch({
      type: "MessageReducer_SendMessage",
    });
  };
};

export const beginChat = (fromUser, toUser) => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({
      type: "MessageReducer_BeginChat",
      from: fromUser,
      to: toUser,
    });
  };
};

export const loadFriendList = friendsData => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({
      type: "FriendListReducer_LOAD_FRIEND_LIST",
      data: friendsData,
    });
  };
};

export const updateSearchFriendData = data => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    dispatch({
      type: "FriendListReducer_UPDATE_SEARCH_DATA",
      data: data,
    });
  };
};

export const searchFriend = () => {
  return (dispatch, getState, { getFirebase, getFirestore }) => {
    const searchData = getState().FriendListReducer.searchData;
    const database = getState().FriendListReducer.databaseData;
    const data = database.filter(item => {
      return item.name.includes(searchData);
    });
    dispatch({
      type: "FriendListReducer_SEARCHING",
      data: data,
    });
  };
};
