import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";
import "firebase/auth";


const config = {
  apiKey: "AIzaSyDdiGFm7mXJYnb2ZofK9Md2Hy_lvLURod8",
  authDomain: "simplechatwithfirebase.firebaseapp.com",
  databaseURL: "https://simplechatwithfirebase.firebaseio.com",
  projectId: "simplechatwithfirebase",
  storageBucket: "simplechatwithfirebase.appspot.com",
  messagingSenderId: "49328739844",
};

firebase.initializeApp(config);
firebase.firestore().settings({timestampsInSnapshots: true});

const storage = firebase.storage();

export {
  storage, firebase as default,
}