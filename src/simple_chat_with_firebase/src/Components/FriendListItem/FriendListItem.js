import React from "react";
import "./FriendListItemStyle.css";
class FriendListItem extends React.Component {
  render() {
    return (
      <div className="FriendListItem_Container">
        <img
          src={this.props.photo}
          alt="Avatar"
          className="FriendListItem_Avatar"
        />
        <div className="FriendListItem_Container2">
          {this.props.toUser !== this.props.email ? (
            <span className="FriendListItem_Name">{this.props.name}</span>
          ) : (
            <span className="FriendListItem_Name" style={{color: 'red'}}>{this.props.name}</span>
          )}
          <div className="FriendListItem_Container3">
            {this.props.status === "online" ? (
              <div className="FriendListItem_Online" />
            ) : (
              <div className="FriendListItem_Offline" />
            )}
            <span className="FriendListItem_Status">{this.props.status}</span>
          </div>
        </div>
      </div>
    );
  }
}

export default FriendListItem;
