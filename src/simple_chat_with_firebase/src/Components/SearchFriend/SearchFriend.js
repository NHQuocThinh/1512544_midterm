import React from "react";
import "./SearchFriendStyle.css";

class SearchFriend extends React.Component {

  searchFriendOnChange = e => {
    const data = e.target.value;
    this.props.updateSearchFriendData(data);
  }

  render() {
    return (
      <input
          style={{
            marginTop: 20,
            padding: 5,
            width: 300,
            height: 30,
            fontSize: 15,
            backgroundColor: "#eff0f4",
            border: "none",
            left: 40,
            borderRadius: 5,
            position: 'absolute'
          }}
          placeholder="Find your friend"
          onChange={this.searchFriendOnChange}
        />
    );
  }
}

export default SearchFriend;
