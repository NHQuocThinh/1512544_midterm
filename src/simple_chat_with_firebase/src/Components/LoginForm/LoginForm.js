import React from "react";
import "./LoginFormStyle.css";

const styles = {
  input: {
    marginBottom: 20,
    padding: 10,
    width: 400,
    height: 30,
    fontSize: 18,
    backgroundColor: "#e0e1e5",
    border: "none",
  },
};

class LoginFrom extends React.Component {
  render() {
    return (
      <div className="login_containerLoginForm">
        <input
          style={styles.input}
          placeholder="Email"
        />
        <input
          style={styles.input}
          placeholder="Password"
          type="password"
        />
        <button
          className="login_loginButtonContainer"
          style={{ color: "white" }}
          onClick={()=>{alert("Comming soon")}}
        >
          LOG IN
        </button>
      </div>
    );
  }
}

export default LoginFrom;
