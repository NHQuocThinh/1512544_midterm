import React from 'react'
import './MessageStyle.css'
class Message extends React.Component {

  messageOnChange = (e) => {
    const tmp = e.target.value;
    let data;
    if(tmp.includes('https://'))
      data = 'ImageLink__'+tmp
    else{
      data = tmp;
    }
    this.props.updateMessage(data);
  }

  uploadImage = (e) => {
    this.props.updateImageUpload(e.target.files[0])
  }

  render(){
    return (
      <div className="Message_Container">
        <textarea className="Message_Input" onChange={this.messageOnChange} value={this.props.message}/>
        <button className="Message_SendButton" onClick={this.props.sendMessage}>SEND</button>
        <input type="file" id="uploadImage" onChange={this.uploadImage} className="Message_UploadImageButton"/>
      </div>
    )
  }
}

export default Message