import React from "react";
import "./SignUpFormStyle.css";

const styles = {
  input: {
    marginBottom: 20,
    padding: 10,
    width: 400,
    height: 30,
    fontSize: 18,
    backgroundColor: "#e0e1e5",
    border: "none",
  },
};

class SignUpForm extends React.Component {
  usernameInputOnChange = e => {
    const data = e.target.value;
    this.props.updateUsernameOnChange(data);
  };

  passwordInputOnChange = e => {
    const data = e.target.value;
    this.props.updatePasswordOnChange(data);
  };

  render() {
    return (
      <div className="containerLoginForm">
        <input
          style={styles.input}
          placeholder="Email"
          onChange={this.usernameInputOnChange}
        />
        <input
          style={styles.input}
          placeholder="Password"
          type="password"
          onChange={this.passwordInputOnChange}
        />
        <button
          className="signup_loginButtonContainers"
          style={{ color: "white" }}
          onClick={() => {
            alert("Comming soon");
          }}
        >
          SIGN UP
        </button>
      </div>
    );
  }
}

export default SignUpForm;
