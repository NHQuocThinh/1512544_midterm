import React from "react";
import "./MessageList.css";

class MessageList extends React.Component {
  render() {
    if (this.props.user === this.props.currentUser) {
      if (this.props.mess.includes("https://")) {
        const imageLink = this.props.mess.slice(11);
        return (
          <div>
            <span className="MessageList_UserStyle">{this.props.user}</span>
            <div className="MessageList_UserMess">
              <img
                src={imageLink}
                alt={this.props.mess}
                className="MessageList_ImageMess"
              />
            </div>
          </div>
        );
      } else {
        return (
          <div>
            <span className="MessageList_UserStyle">{this.props.user}</span>
            <div className="MessageList_UserMess">
              <span className="MessageList_UserMessStyle">
                {this.props.mess}
              </span>
            </div>
          </div>
        );
      }
    } else {
      if (this.props.mess.includes("https://")) {
        const imageLink = this.props.mess.slice(11);
        return (
          <div>
            <span className="MessageList_FriendStyle">{this.props.user}</span>
            <div className="MessageList_FriendMess">
              <img
                src={imageLink}
                alt={this.props.mess}
                className="MessageList_ImageMess"
              />
            </div>
          </div>
        );
      } else {
        return (
          <div>
            <span className="MessageList_FriendStyle">{this.props.user}</span>
            <div className="MessageList_FriendMess">
              <span className="MessageList_FriendMessStyle">
                {this.props.mess}
              </span>
            </div>
          </div>
        );
      }
    }
  }
}

export default MessageList;
