import React from "react";
import { connect } from "react-redux";
import "./UploadingImageStyle.css";

class UploadingImage extends React.Component {
  render() {
    return (
      <div className="UploadingImage_Container">
        <span className="UploadingImage_Text">Uploading your image</span>
        <progress
          value={this.props.progress}
          max="100"
          style={{ marginTop: 30, height: 30, width: 200 }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return{
    progress: state.MessageReducer.uploadingPercent
  }
};

const mapDispatchToProps = dispatch => {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UploadingImage);
